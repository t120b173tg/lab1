package org.copper.ktu.dsp.ld1

import jade.core.Agent

class SimpleAgent : Agent() {

    override fun setup() {
        println("Hello, I'm $localName and I'm living in ${containerController.name}")
    }
}