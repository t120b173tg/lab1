package org.copper.ktu.dsp.ld12

import jade.core.Agent
import kotlinx.coroutines.experimental.launch

class AgentLauncher : Agent() {
    override fun setup() {
        println("A[$localName] is starting")

        println("A[$localName] Launching simple agent")

        try {
            containerController
            .createNewAgent("Mano1", SimpleAgent::class.java.name, null)
            .start()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            Thread.sleep(5000)    // sleep 5 s
        } catch(e: InterruptedException) {}

        launch {
            shutdownJadePlatform()
        }
    }

    private fun shutdownJadePlatform() {
        try {
            println("A[$localName] Shut down Jade Platform")

            containerController.kill()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}