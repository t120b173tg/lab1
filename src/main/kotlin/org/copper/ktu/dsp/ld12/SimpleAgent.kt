package org.copper.ktu.dsp.ld12

import jade.core.Agent

class SimpleAgent : Agent() {

    override fun setup() {
        println("Hello, I'm $localName and I'm living in ${containerController.name}")
    }

    override fun takeDown() {
        println("A[$localName] is being removed")
        println("Saving data to files\nBye")
    }
}