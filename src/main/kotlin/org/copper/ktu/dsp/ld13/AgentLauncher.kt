package org.copper.ktu.dsp.ld13

import jade.core.Agent

class AgentLauncher : Agent() {
    public override fun setup() {
        println("A[$localName] is starting")

        val param = (arguments.getOrElse(0, { "1" }) as String).toInt()

        println("A[$localName] Launching $param simple agents")

        try {
            for (i in 0 until param) {
                val args2 = arrayOf("DATA$i:", ((i + 1) * 1000))
                containerController
                    .createNewAgent("Mano$i", ParamAgent2::class.java.name, args2)
                    .start()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun shutdownJadePlatform() {
        try {
            println("A[$localName] Shut down Jade Platform")

            containerController.kill()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}