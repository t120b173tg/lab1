package org.copper.ktu.dsp.ld13

import jade.core.Agent

class ParamAgent2 : Agent() {

    override fun setup() {
        println("A[$localName and I'm  living in ${containerController.name}")
        val time = (arguments.getOrElse(1, { 1000 }) as Int).toLong()

        for (i in 0..7) {
            try {
                Thread.sleep(time)
                println("A[$localName] $time [$i/10] sleep for ${time / 1000}s")
            } catch (e: InterruptedException) {
            }
        }

        this.doDelete()
    }

    override fun takeDown() {
        println("A[$localName] is being removed")
        println("Saving data to files\nBye")
    }
}